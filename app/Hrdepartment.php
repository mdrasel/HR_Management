<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hrdepartment extends Model
{
    protected $fillable = [
        'name'
    ];

    public function hremployee(){
        return $this->hasOne('App\Hremployee');
    }
}
