<?php

namespace App\Http\Controllers;
use App\Hrdesignation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HrdesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hrdesignation=Hrdesignation::all();

        return view('admin.designation.index',['designations'=>$hrdesignation]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->only('name');
        $hrdesignation=Hrdesignation::create($data);
        Session::flash('message','Designation Successfully Inserted');
        return redirect('/hrdesignations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hrdesignation=Hrdesignation::find($id);
        return view('admin.designation.view',['designation'=>$hrdesignation]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hrdesignation=Hrdesignation::findorfail($id);
        return view('admin.designation.edit',['designation'=>$hrdesignation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hrdesignation= Hrdesignation::find($id);
        $data = $request->only('name');
        $hrdesignation->update($data);
        Session::flash('message', 'Designation Updated Successfully');
        return redirect('/hrdesignations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hrdesignation=Hrdesignation::find($id);
        $name=$hrdesignation->name;
        $hrdesignation->destroy($id);
        Session::flash('message'," $name Successfully Deleted");
        return redirect('/hrdesignations');
    }
}
