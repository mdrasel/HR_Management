<?php

namespace App\Http\Controllers;

use App\Hrdepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HrdepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hrdepartments=Hrdepartment::all();

        return view('admin.department.index',['departments'=>$hrdepartments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->only('name');
        $hrdepartment=Hrdepartment::create($data);
        Session::flash('message','Department Successfully Inserted');
        return redirect('/hrdepartments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hrdepartment=Hrdepartment::find($id);
        return view('admin.department.view',['department'=>$hrdepartment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hrdepartment=Hrdepartment::findorfail($id);
        return view('admin.department.edit',['department'=>$hrdepartment]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hrdepartment= Hrdepartment::find($id);
        $data = $request->only('name');
        $hrdepartment->update($data);
        Session::flash('message', 'Department Updated Successfully');
        return redirect('/hrdepartments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hrdepartment=Hrdepartment::find($id);
        $name=$hrdepartment->name;
        $hrdepartment->destroy($id);
        Session::flash('message'," $name Successfully Deleted");
        return redirect('/hrdepartments');
    }
}
