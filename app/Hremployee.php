<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hremployee extends Model
{
    public function hrdepartment(){
        return $this->belongsTo('App\Hrdepartment');
    }

    public function hrdesignation(){
        return $this->belongsTo('App\Hrdesignation');
    }

}
