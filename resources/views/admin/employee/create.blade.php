@extends('admin.layouts.master')
@section('title','HR Management || Employee Add')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-haze">
                            <i class="icon-settings font-green-haze"></i>
                            <span class="caption-subject bold uppercase"> Add Employee</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!! Form::open(['url' => '/hremployees','class'=>'form-horizontal']) !!}
                        <div class="form-body">
                            <div class="form-group form-md-line-input has-success">
                                {!! Form::label('employee_identy','Employee ID',['class'=>'col-md-2 control-label']) !!}
                                <div class="col-md-10">
                                    {!! Form::text('employee_identy',null,['class'=>'form-control','id'=>'form_control_1','placeholder'=>'Enter employee ID']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input has-success">
                                {!! Form::label('card_no','Card No.',['class'=>'col-md-2 control-label']) !!}
                                <div class="col-md-10">
                                    {!! Form::text('card_no',null,['class'=>'form-control','id'=>'form_control_1','placeholder'=>'Enter card no.']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input has-success">
                                <label class="col-md-2 control-label" for="form_control_1">Department</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="">
                                        <option value="">Option 1</option>
                                        <option value="">Option 2</option>
                                        <option value="">Option 3</option>
                                    </select>
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">Some help goes here...</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input has-success">
                                <label class="col-md-2 control-label" for="form_control_1">Designation</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="">
                                        <option value="">Option 1</option>
                                        <option value="">Option 2</option>
                                        <option value="">Option 3</option>
                                    </select>
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">Some help goes here...</span>
                                </div>
                            </div>
                            <div class="form-group form-md-radios has-warning">
                                <label class="col-md-2 control-label" for="form_control_1">Status</label>
                                <div class="col-md-10">
                                    <div class="md-radio-inline">
                                        <div class="md-radio">
                                            {!! Form::radio('status','Y',['class'=>'md-radiobtn','id'=>'checkbox1_8']) !!}


                                            <label for="checkbox1_8">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Active </label>
                                        </div>
                                        <div class="md-radio">
                                            {!! Form::radio('status','N',['class'=>'md-radiobtn','id'=>'checkbox1_9','checked'=>'']) !!}

                                            <label for="checkbox1_9">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Inactive </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    {{--     <button type="button" class="btn default">Cancel</button>--}}
                                    {!! Form::submit('Submit',['class'=>'btn blue']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
@endsection