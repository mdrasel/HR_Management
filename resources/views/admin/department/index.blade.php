@extends('admin.layouts.master')
@section('title','HR Management || Department All')
@section('content')
    <div class="page-content">
<div class="row">
    @if(Session::has('message'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            {!!Session::get('message')!!}
        </div>
    @endif
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-comments"></i>Department List </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="javascript:;" class="reload"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th> SL No. </th>
                            <th> Name </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($departments as $department)
                        <tr>
                            <td> {{$department->id}}</td>
                            <td> {{$department->name}} </td>
                            <td>
                                <a href="{{url('/hrdepartments/'.$department->id)}}" class="btn btn-outline btn-circle dark btn-sm black">View</a>|
                                <a href="{{url('/hrdepartments/'.$department->id.'/edit')}}" class="btn btn-outline btn-circle green btn-sm purple">Edit</a>|

                                {{ Form::open(['method'=> 'DELETE','url' => ['/hrdepartments', $department->id]]) }}

                                {{ Form::submit('Delete',['class'=>'btn btn-outline btn-circle red btn-sm blue']) }}

                                {{ Form::close() }}

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
    </div>
@endsection