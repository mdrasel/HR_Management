@extends('admin.layouts.master')
@section('title','HR Management || Department Edit')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-haze">
                            <i class="icon-settings font-green-haze"></i>
                            <span class="caption-subject bold uppercase"> Edit Department</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!! Form::open(['method'=>'PATCH','url' => ['/hrdepartments',$department->id],'class'=>'form-horizontal']) !!}
                        <div class="form-body">
                            <div class="form-group form-md-line-input">
                                {!! Form::label('name','Department',['class'=>'col-md-2 control-label']) !!}
                                <div class="col-md-10">
                                    {!! Form::text('name',$department->name,['class'=>'form-control','id'=>'form_control_1','placeholder'=>'Enter department name']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    {{--     <button type="button" class="btn default">Cancel</button>--}}
                                    {!! Form::submit('Submit',['class'=>'btn blue']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </div>
@endsection