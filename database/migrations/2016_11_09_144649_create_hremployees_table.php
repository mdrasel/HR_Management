<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHremployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hremployees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designation_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('employee_identy');
            $table->integer('card_no');
            $table->date('join_date');
            $table->date('termination_date');
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('designation_id')->references('id')->on('hrdesignations')->onDelete('restrict');
            $table->foreign('department_id')->references('id')->on('hrdepartments')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hremployees');
    }
}
